import 'package:flutter/material.dart';

void main() {
  runApp(const Home());
}

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: HomeScreen(), // For Cleaner code
    );
  }
}

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int number = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("We are learning about container"),
      ),
      body: Container(
        color: Colors.pink,
        //padding: const EdgeInsets.all(1),
        width: 200,
        height: 300,
        alignment: Alignment.center,
        child: const Text('Flutter is the best'),
      ),
    );
  }
}
